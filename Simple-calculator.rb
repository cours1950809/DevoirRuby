class SimpleCalculator

    def self.calculate(firstNumber, secondNumber, operator)
      if firstNumber.class != String && secondNumber.class != String && operator.class == String
        result = 0
        if operator == "+"
          result = firstNumber + secondNumber
        elsif operator == "-"
          result = firstNumber - secondNumber
        elsif operator == "*"
          result = firstNumber * secondNumber
        elsif operator == "/"
          if firstNumber != 0 && secondNumber != 0
            result = firstNumber / secondNumber
          else
            raise("Division by zero is not allowed")
          end
        else
          raise("UnsupportedOperation")
        end
        puts("#{firstNumber} #{operator} #{secondNumber} = #{result}")
      else
        raise("ArgumentError")
      end
    end

  end

  SimpleCalculator.calculate(4, 2, '*')