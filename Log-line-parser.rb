class LogLineParser

    def initialize(s)
      @message = s
    end
  
    def message
      start = @message.index(": ") + 2
      ending = @message.index("\r")
      if (ending == nil)
        ending = @message.index("\n")
      elsif (ending == nil)
        ending = @message.length -1
      end
      @message[start...ending]
    end
  
    def log_level
      start = @message.index("[") + 1
      ending = @message.index("]")
      @message[start...ending].downcase
    end
  
    def reformat
      msg = self.message
      log = self.log_level
      puts "#{msg} (#{log})"
    end
  end
  
  LogLineParser.new("[WARNING]: Disk almost full\r\n").reformat
  LogLineParser.new('[INFO]: Operation completed').reformat
  