class Microwave
    def initialize(sec)
      @second = sec.to_i
    end
  
    def timer
      rs = @second % 60
      rm = (@second - rs) / 60
      puts "#{self.display(rm)}:#{self.display(rs)}"
    end
  
    def display(nbr)
      if (nbr.digits.length == 1)
        "0#{nbr}"
      else
        "#{nbr}"
      end
    end
  end
  
  Microwave.new(2).timer
  